Arch Linux Installation
=======================

* Boot an existing Arch Linux system or `install image <https://www.archlinux.org/download/>`_ that has access to the target installation medium and run:
  ::

    [root ~]$ wget https://bitbucket.org/bbharris/arch-install/get/master.tar.gz -O - | tar xz
    [root ~]$ ./bbharris-arch-install-<tab>/arch-install.sh strap

* Reboot to the target installation medium and run:
  ::

    [root ~]$ /arch-install.sh root
    [bharris ~]$ /arch-install.sh user
    [bharris ~]$ /arch-install.sh aur
    [bharris ~]$ /arch-install.sh vbox # optional
    [bharris ~]$ /arch-install.sh nvidia # optional