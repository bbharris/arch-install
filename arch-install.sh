#!/bin/bash -u
cmd0=""
trap 'cmd1=$cmd0; cmd0=$BASH_COMMAND' DEBUG
function prompt {
  while true; do
    read -p "$1 (yes/[skip]/shell/exit) "
    if [[ "$REPLY" == "" ]]; then REPLY=skip; fi
    case $REPLY in yes|skip) break;; shell) bash;; exit) exit 1;; *);; esac
  done
}
function check {
  while true; do
    echo "Executing ($1)..."
    bash -c "$1"
    if [ $? -eq 0 ]; then break; fi
    prompt "Failed to execute command, retry?"
    if [[ "$REPLY" == "skip" ]]; then break; fi
  done
}
function input {
  while true; do
    read -p "$2 "
    $1 "$REPLY"
    if [ $check -eq 1 ]; then break; fi
  done
}
if [ $1 == "strap" ]; then
  function check_host {
    HOST="bharris-arch-$1"
    if [[ "$1" != "" ]]; then check=1; else check=0; fi
  }
  input check_host "Enter a hostname suffix:"
  pacman -Q arch-install-scripts
  if [ $? -ne 0 ]; then
    check "pacman -Sy arch-install-scripts"
  fi
  function check_zone {
    ZONE="$1"
    if [[ "$1" != "" ]]; then check=1; else check=0; fi
  }
  input check_zone "Enter a timezone:"
  check "timedatectl set-ntp true"
  check "timedatectl set-timezone $ZONE"
  LSBLK_OUTS=name,label,size,model,fstype,mountpoint
  function check_disk {
    DISK="/dev/$1"
    if [[ `lsblk -nd --output type $DISK` == "disk" ]]; then check=1; else check=0; fi
  }
  lsblk --output $LSBLK_OUTS
  input check_disk "Which disk?"
  lsblk --output $LSBLK_OUTS $DISK
  prompt "Partition $DISK with fdisk?"
  if [[ "$REPLY" == "yes" ]]; then
    check "fdisk $DISK"
  fi
  function check_part {
    PART="$DISK""$1"
    if [[ `lsblk -nd --output type $PART` == "part" ]]; then check=1; else check=0; fi
  }
  lsblk --output $LSBLK_OUTS $DISK
  input check_part "Which partition number?"
  lsblk --output $LSBLK_OUTS $PART
  prompt "Format $PART to ext4 (WARNING: DATA LOSS!)?"
  if [[ "$REPLY" == "yes" ]]; then
    check "mkfs.ext4 $PART"
    check "e2label $PART $HOST"
  fi
  check "mkdir -p /mnt"
  check "mount $PART /mnt"
  function unmount {
    umount /mnt
  }
  trap unmount EXIT
  echo "contents of $PART:"
  ls /mnt
  prompt "Install base packages to $PART (WARNING: DATA LOSS!)?"
  if [[ "$REPLY" == "yes" ]]; then
    check "pacstrap /mnt base linux linux-firmware vi"
  fi
  check "genfstab -U /mnt > /mnt/etc/fstab"
  check "cp $0 /mnt/arch-install.sh"
  arch-chroot /mnt /arch-install.sh chroot $DISK $HOST $ZONE
elif [ $1 == "chroot" ]; then
  DISK=$2
  HOST=$3
  ZONE=$4
  check "ln -sf /usr/share/zoneinfo/$ZONE /etc/localtime"
  check "hwclock --systohc"
  check "sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen"
  check "locale-gen"
  if [ ! -e /etc/local.conf ]; then
    check "echo LANG=en_US.UTF-8 > /etc/locale.conf"
  fi
  if [ ! -e /etc/hostname ]; then
    check "echo $HOST > /etc/hostname"
  fi
  prompt "Reconfigure linux image (e.g. reorder block hook for USB)?"
  if [[ "$REPLY" == "yes" ]]; then
    check "vi /etc/mkinitcpio.conf" # move block directly after udev for usb install
    check "mkinitcpio -p linux"
  fi
  check "pacman -S netctl dhcpcd iw wpa_supplicant dialog intel-ucode grub"
  prompt "Install grub to $DISK?"
  if [[ "$REPLY" == "yes" ]]; then
    check "grub-install --target=i386-pc $DISK"
    check "grub-mkconfig -o /boot/grub/grub.cfg"
  fi
  check "passwd"
elif [ $1 == "root" ]; then
  prompt "Configure wired network?"
  if [[ "$REPLY" == "yes" ]]; then
    check "systemctl start dhcpcd"
    check "systemctl enable dhcpcd"
  fi
  prompt "Configure wireless network?"
  if [[ "$REPLY" == "yes" ]]; then
    check "wifi-menu"
    function check_wifi {
      netctl enable $1
      if [ $? -eq 0 ]; then check=1; else check=0; fi
    }
    netctl list
    input check_wifi "Which wifi profile do you wish to use?"
  fi
  check "timedatectl set-ntp true"
  prompt "Install common packages?"
  if [[ "$REPLY" == "yes" ]]; then
    check "pacman -S \
      sudo polkit fakeroot \
      xf86-video-fbdev xorg-server xorg-xinit awesome alsa-utils \
      xterm gvim tree regexxer chromium zathura zathura-pdf-mupdf \
      git openssh wget \
      make tup pkg-config flex clang valgrind gdb"
  fi
  prompt "Make tup privileged?"
  if [[ "$REPLY" == "yes" ]]; then
    check "chmod u+s /usr/bin/tup"
    check "sed -i 's/#user_allow_other/user_allow_other/' /etc/fuse.conf"
  fi
  prompt "Configure sudo (e.g. allow wheel)?"
  if [[ "$REPLY" == "yes" ]]; then
    check "visudo"
  fi
  id -u bharris
  if [ $? -ne 0 ]; then
    check "useradd -m -G wheel -s /bin/bash bharris"
    check "passwd bharris"
  fi
elif [ $1 == "user" ]; then
  cd
  prompt "Setup dotfiles?"
  if [[ "$REPLY" == "yes" ]]; then
    check "rm .bashrc .bash_profile"
    check "git clone --bare https://bbharris@bitbucket.org/bbharris/dotfiles.git ~/.dotfiles.git"
    gitc="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
    check "$gitc checkout"
    check "$gitc config --local status.showUntrackedFiles no"
  fi
  if [ ! -f ~/.ssh/id_rsa ]; then
    check "ssh-keygen -f ~/.ssh/id_rsa"
  fi
elif [ $1 == "aur" ]; then
  check "rm -rf /tmp/aurget"
  check "cd /tmp && wget https://aur.archlinux.org/cgit/aur.git/snapshot/aurget.tar.gz -O - | tar xz"
  check "cd /tmp/aurget && makepkg -s"
  check "sudo pacman -U /tmp/aurget/aurget-*.pkg.tar.xz"
elif [ $1 == "vbox" ]; then
  check "pacman -S virtualbox-guest-utils"
  check "systemctl enable vboxservice"
elif [ $1 == "nvidia" ]; then
  check "pacman -S nvidia nvidia-settings cuda"
  prompt "Configure Optimus?"
  if [[ "$REPLY" == "yes" ]]; then
    cat > /etc/X11/xorg.conf.d/20-nvidia.conf <<- EOF
Section "Module"
  Load "modesetting"
EndSection
Section "Device"
  Identifier "nvidia"
  Driver "nvidia"
  BusID "1:0:0"
  Option "AllowEmptyInitialConfiguration"
EndSection
EOF
  fi
else
  echo "Unknown script command $1"
  exit 1
fi
